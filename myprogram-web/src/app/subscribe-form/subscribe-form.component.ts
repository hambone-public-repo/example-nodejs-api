import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-subscribe-form',
  templateUrl: './subscribe-form.component.html',
  styleUrls: ['./subscribe-form.component.scss']
})
export class SubscribeFormComponent implements OnInit {

  subscribeFormGroup: FormGroup;

  constructor(private userService: UserService) { 
    this.subscribeFormGroup = new FormGroup({
      userName: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      phone: new FormControl('', [validateNumeric]),
    })
  }

  ngOnInit(): void {
  }

  onSubscribeSubmit(): void {
    console.log(this.subscribeFormGroup.value);

    this.userService.postUser(this.subscribeFormGroup.value).subscribe(x => {
      console.log('update success');
      
    });
    
  }

}

export function validateNumeric(control: AbstractControl) {
  const v = +control.value;
  return v == null || Number.isNaN(v) ? {valid: false} : null;
}
