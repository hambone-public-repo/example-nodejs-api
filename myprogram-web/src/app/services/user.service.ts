import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly endpoint = '/api/user'

  constructor(private http: HttpClient) { }

   postUser(user: User): Observable<any> {
    return this.http.post(this.endpoint, user)
  }

}

export interface User {
  userName?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  phone?: string | null;
}
