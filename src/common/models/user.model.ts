export interface User {
    userName?: string | null;
    password?: string | null;
    firstName?: string | null;
    lastName?: string | null;
    phone?: string | null;
}
