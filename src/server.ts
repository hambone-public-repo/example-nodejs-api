import express, { json } from 'express';
import { banner, consoleGreen } from '@common/banner.util';
import { initializeDb } from '@data/user.repository';
import { userRouter } from '@routes/user.route';

banner('Hambone-Fett');
banner('Example');
banner('API');

const server = express();
server.use(json());
const port = process.env.port || '8888';

await initializeDb();

server.use('/api/', userRouter);
consoleGreen('user route has been loaded');

server.listen(port);

consoleGreen(`server has started in local port ${port}`);
